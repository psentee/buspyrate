"""\
Python interface for BusPirate binary mode
==========================================

[GitLab Project][gitlab] | [PyPi Package][pypi] | [API Documentation][pages]

[gitlab]: https://gitlab.com/psentee/buspyrate
[pypi]: https://pypi.org/project/buspyrate/
[pages]: https://psentee.gitlab.io/buspyrate
"""

__all__ = ["BusPirate", "Data", "SPISpeed"]

from .buspirate import BusPirate
from .const import SPISpeed
from .typing import Data
