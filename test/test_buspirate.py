from time import sleep

from buspyrate import BusPirate, SPISpeed
from buspyrate.const import CONFIRM

from .util import BusPirateThread


def test_reset():
    def run(bp: BusPirate) -> None:
        bp.reset()

    with BusPirateThread(run) as sa:
        assert sa.recv(1) == b"\x00"
        sa.send(b"BBIO1")
        assert sa.recv(1) == b"\x0F"
        sa.send(CONFIRM)


def test_with_and_spi():
    def run(bp: BusPirate) -> None:
        with bp:
            bp.spi(SPISpeed.S125k)

    with BusPirateThread(run) as sa:
        # Bitbang enter (succeed at third zero)
        assert sa.recv(1) == b"\x00"
        sleep(0.067)
        assert sa.recv(1) == b"\x00"
        sa.send(b"asdfg")
        assert sa.recv(1) == b"\x00"
        sa.send(b"BBIO1")

        # SPI mode
        assert sa.recv(1) == b"\x01"
        sa.send(b"SPI1")

        # SPI speed 125k
        assert sa.recv(1) == b"\x61"
        sa.send(CONFIRM)

        # SPI config (clock edge falling 1, rest 0)
        assert sa.recv(1) == b"\x82"
        sa.send(CONFIRM)

        # Periph config: power & CS on
        assert sa.recv(1) == b"\x49"
        sa.send(CONFIRM)

        # Reset sequence
        assert sa.recv(1) == b"\x00"
        sa.send(b"BBIO1")
        assert sa.recv(1) == b"\x0F"
        sa.send(CONFIRM)
