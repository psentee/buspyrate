"""Test utilities."""

import socket
import typing as t
from threading import Thread

from buspyrate import BusPirate


class SocketSerial:
    """Socket-backed implementation of SerialDuck interface."""

    s: socket.socket

    def __init__(self, s: socket.socket) -> None:
        """Create new serial-quacking socket wrapper."""
        self.s = s

    @property
    def timeout(self) -> t.Optional[float]:
        """Get or set 'serial' timeout."""
        return self.s.gettimeout()

    @timeout.setter
    def timeout(self, timeout: t.Optional[float]):
        self.s.settimeout(timeout)

    def close(self) -> None:
        """Close connection."""
        self.s.close()

    def read(self, size: int = 1) -> bytes:
        """Read `size` bytes from 'serial'."""
        try:
            return self.s.recv(size)
        except (OSError, socket.timeout):
            return b""

    def write(self, data: bytes) -> None:
        """Write data to 'serial'."""
        self.s.sendall(data)

    def flush(self) -> None:
        """Pretend to flush 'serial'."""
        pass


class BusPirateThread(Thread):
    """Test helper thread that runs buspyrate.BusPirate side of communication.

    Used as a context manager, yields a socket for the BusPirate board
    side of communication, letting the main thread pretend to be
    hardware.

    Exceptions (including failed assertions) from the helper thread
    will be re-raised in the main thread on join.
    """

    _run: t.Tuple[t.Callable[[BusPirate], None]]
    _bp_exc: t.Optional[Exception] = None
    bp: BusPirate

    def __init__(self, run: t.Callable[[BusPirate], None]) -> None:
        """Create a new BusPirate thread.

        The `run` argument should be a function that accepts a
        BusPirate instance and executes the test scenario.
        """
        self._run = (run,)
        sa, sb = socket.socketpair()
        ser = SocketSerial(sb)
        ser.timeout = 0.05
        self.bp = BusPirate(ser)
        self.sock = sa
        super().__init__(name="BusPirate")
        self.daemon = True

    def __enter__(self) -> socket.socket:
        """Start thread and yield socket for the 'board' side of communication."""
        self.start()
        return self.sock

    def __exit__(self, exc_type, exc_value, traceback) -> None:
        """Join thread, reraise any exceptions."""
        # TODO: reliably terminate thread?
        self.join(1)
        assert not self.is_alive()
        if self._bp_exc is not None:
            raise self._bp_exc

    def run(self):
        """Run the scenario function, saving the exception if raised."""
        try:
            self._run[0](self.bp)
        except Exception as exc:
            self._bp_exc = exc
